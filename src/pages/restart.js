import React, { Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import $ from 'jquery'
import QuestionData from './questions.json'

let restoredScore = JSON.parse(localStorage.getItem('score'));


export const Restart = () => {
    function showResults() {
        $('.textResult').text('Тест завершен: вы правильно ответили на ' + restoredScore + ' из ' + QuestionData.length + ' вопросов');
    }
    return (
        <Fragment>
            <div className='main'>
                <button className='main-Navlink' onClick={showResults}>Посмотреть результаты</button>
                <div className='textResult'>Тест пройден</div>
                <NavLink className='main-Navlink' to='/QuesAnswer'>
                    Пройти заново
                </NavLink>
            </div>
        </Fragment>
    )
}