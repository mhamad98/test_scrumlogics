import React, { Fragment } from 'react';
import $ from 'jquery';
import QuestionData from './questions.json'

let score = 0;
export function Start() {
    let level = 0;
    function show(level) {
        $('.CorrectAnswers').text('пройденно ' + level + ' вопроса из ' + QuestionData.length);
        $('.question').text(QuestionData[level].questions);
        $('label[for=answer1]').text(QuestionData[level].answer[level*3 +0]);
        $('label[for=answer2]').text(QuestionData[level].answer[level*3 +1]);
        $('label[for=answer3]').text(QuestionData[level].answer[level*3 +2]);
    }
    show(level);
    $('.main-Navlink').click(function () {
        if (level < QuestionData.length - 1) {
            if ($('input[name=answer]:checked').val() == QuestionData[level].key) {
                level++;
                show(level);
                score++;
            }
            else {
                level++;
                show(level);
            }
            $('input').prop('checked', false);
        }
        else {
            if ($('input[name=answer]:checked').val() == QuestionData[level].key) {
                score++;
            }
            document.location.href = "http://localhost:3000/Restart";

        }
        localStorage.setItem('score', JSON.stringify(score));
        console.log(score)
    })
}

export const QuesAnswer = () => {
    return (
        <div className='QuestionAnswer'>
            <button className='main-Navlink' onClick={Start}>START</button>
            <h6 className='CorrectAnswers'></h6>
            <h2 className="question"></h2>
            <div className='Answers'>
                <input className="answer-input" type="radio" name="answer" id="answer1" value="0" />
                <label htmlFor="answer1" className="answer-label"></label>
            </div>
            <div className='Answers'>
                <input className="answer-input" type="radio" name="answer" id="answer2" value="1" />
                <label htmlFor="answer2" className="answer-label"></label>
            </div>
            <div className='Answers'>
                <input className="answer-input" type="radio" name="answer" id="answer3" value="2" />
                <label htmlFor="answer3" className="answer-label"></label>
            </div>
            <div className='Results'></div>
            <input className='main-Navlink' type='button' value='ПОДТВЕРДИТЬ' />
        </div>
    )
}
