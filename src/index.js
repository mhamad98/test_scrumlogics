import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
const question = ['Что из этого не является косметическим средством?', 'Кто, в конце концов, скушал Колобка?', 'Какой шахматной фигуры не существует?'];
const answer = ['Помада', 'Татуировка', 'Крем', 'Пудра', 'Дед', 'Баба', 'Заяц', 'Лиса', 'Пешка'];
const key = [1, 1, 1];

ReactDOM.render(<App />, document.getElementById('root'));
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();


