import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import {Home} from './pages/home'
import {QuesAnswer} from './pages/QuesAnswer'
import {Restart} from './pages/restart'

function App() {
  return (
    <BrowserRouter>
      <div className="container">
      <Switch>
        <Route path={'/'} exact component={Home} />
        <Route path={'/QuesAnswer'} exact component={QuesAnswer} />
        <Route path={'/Restart'} exact component={Restart} />
      </Switch>
      </div>
    </BrowserRouter>
  );
}
export default App;
